package fmt

import (
	"fmt"
	"strings"

	"temporary.design/jack-compiler/internal/tokenizer"
)

type TokenStringBuilder struct {
	tokens []tokenizer.Token
}

func FormatTokens(tokens []tokenizer.Token) string {
	output := strings.Builder{}

	output.WriteString("<tokens>")
	output.WriteString("</tokens>")

	return output.String()
}

func (t *TokenStringBuilder) AddToken(tk tokenizer.Token) {
	t.tokens = append(t.tokens, tk)
}

func (t *TokenStringBuilder) Slice() []string {
	output := []string{}

	output = append(output, "<tokens>")

	for _, tk := range t.tokens {
		o := fmt.Sprintf(`<%s> %s </%s>`, tk.Type.Classifier(), tk.Value, tk.Type.Classifier())
		output = append(output, o)
	}

	output = append(output, "</tokens>")

	return output
}

package fmt

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"temporary.design/jack-compiler/internal/tokenizer"
)

func TestTokenListFormatting(t *testing.T) {
	tokens := []tokenizer.Token{
		{
			Type:  tokenizer.TOK_KEYWORD,
			Value: "class",
		},
		{
			Type:  tokenizer.TOK_IDENTIFIER,
			Value: "Main",
		},
		{
			Type:  tokenizer.TOK_LEFT_BRACKET_CURLY,
			Value: "{",
		},
	}

	tokenFormatter := TokenStringBuilder{}

	for _, tk := range tokens {
		tokenFormatter.AddToken(tk)
	}

	actual := tokenFormatter.Slice()

	expect := []string{
		"<tokens>",
		"<keyword> class </keyword>",
		"<identifier> Main </identifier>",
		"<symbol> { </symbol>",
		"</tokens>",
	}

	if diff := cmp.Diff(expect, actual); diff != "" {
		t.Errorf("FormatTokens() mismatch (-want +got):\n%s", diff)
	}
}

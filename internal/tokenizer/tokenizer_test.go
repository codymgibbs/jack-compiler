package tokenizer

import "testing"

func TestGetsNextToken(t *testing.T) {
	AssertNextTokenTypeMatches(t, `42`, TOK_NUMBER)
}

func TestGetsNextTokenIgnoresCommentsAndWhiteSpace(t *testing.T) {
	input := `
		// This isn't necessary
		/*
			Neither is this
		*/

		42
	`

	AssertNextTokenTypeMatches(t, input, TOK_NUMBER)
}

func TestTokenContainsValue(t *testing.T) {
	options := []string{"foo", "bar", "baz"}

	// No match
	token := Token{TOK_IDENTIFIER, "fizz"}
	found := token.ContainsValueIn(options...)
	AssertIs(t, false, found)

	// With match
	token = Token{TOK_IDENTIFIER, "bar"}
	found = token.ContainsValueIn(options...)
	AssertIs(t, true, found)
}
func TestFindTokenValue(t *testing.T) {
	options := []string{"foo", "bar", "baz"}

	// No match
	token := Token{TOK_IDENTIFIER, "fizz"}
	found := token.FindValue(options)
	AssertStringMatch(t, "", found)

	// With match
	token = Token{TOK_IDENTIFIER, "bar"}
	found = token.FindValue(options)
	AssertStringMatch(t, "bar", found)
}

func AssertIs(t *testing.T, expect bool, actual bool) {
	t.Helper()

	if expect != actual {
		t.Errorf(`Got: "%v", got: "%v"`, actual, expect)
	}
}

func AssertStringMatch(t *testing.T, expect string, actual string) {
	t.Helper()

	if expect != actual {
		t.Errorf(`Got unexpected value: "%s", got: "%s"`, actual, expect)
	}
}

func AssertNextTokenTypeMatches(t *testing.T, input string, expected TokenType) {
	t.Helper()

	tokenizer := NewTokenizer()

	tokenizer.Init(input)

	tk, err := tokenizer.GetNextToken()
	if err != nil {
		t.Error(err)
	}

	if tk.Type != expected {
		t.Errorf(`Got unexpected token type: "%s"`, tk.Type.ToString())
	}
}

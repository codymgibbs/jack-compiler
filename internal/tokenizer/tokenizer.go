package tokenizer

import (
	"fmt"
	"regexp"
)

type TokenType int

const (
	TOK_IGNORE = TokenType(iota)

	TOK_IDENTIFIER
	TOK_KEYWORD
	TOK_NUMBER
	TOK_STRING

	// Symbols
	TOK_DOT
	TOK_SEMI
	TOK_LEFT_BRACKET_CURLY
	TOK_RIGHT_BRACKET_CURLY
	TOK_RIGHT_PAREN
	TOK_LEFT_PAREN
)

func (t TokenType) ToString() string {
	switch t {
	case TOK_IDENTIFIER:
		return "IDENTIFIER"
	case TOK_KEYWORD:
		return "KEYWORD"
	case TOK_NUMBER:
		return "NUMBER"
	case TOK_STRING:
		return "STRING"

	// Symbols
	case TOK_SEMI:
		return ";"
	case TOK_LEFT_BRACKET_CURLY:
		return "{"
	case TOK_RIGHT_BRACKET_CURLY:
		return "}"
	case TOK_LEFT_PAREN:
		return "("
	case TOK_RIGHT_PAREN:
		return ")"

	// IGNORED
	default:
		return ""
	}
}

func (t TokenType) Classifier() string {
	switch t {
	case TOK_IDENTIFIER:
		return "identifier"
	case TOK_KEYWORD:
		return "keyword"
	case TOK_NUMBER:
		return "number"
	case TOK_STRING:
		return "string"

	// Symbols
	case TOK_SEMI:
		return "symbol"
	case TOK_LEFT_BRACKET_CURLY:
		return "symbol"
	case TOK_RIGHT_BRACKET_CURLY:
		return "symbol"
	case TOK_LEFT_PAREN:
		return "symbol"
	case TOK_RIGHT_PAREN:
		return "symbol"

	// IGNORED
	default:
		return ""
	}
}

type tokenSpec struct {
	re        *regexp.Regexp
	tokenType TokenType
}

var tokenSpecs []tokenSpec

type Token struct {
	Type  TokenType
	Value string
}

func (t *Token) ContainsValueIn(options ...string) bool {
	for _, v := range options {
		if v == t.Value {
			return true
		}
	}

	return false
}

func (t *Token) FindValue(options []string) string {
	for _, v := range options {
		if v == t.Value {
			return v
		}
	}

	return ""
}

type Tokenizer struct {
	input  string
	cursor int
}

func (t *Tokenizer) Init(input string) {
	t.input = input
	t.cursor = 0
}

func (t *Tokenizer) GetNextToken() (Token, error) {
	if !t.HasMoreTokens() {
		return Token{}, nil
	}

	current := t.input[t.cursor:]

	for _, spec := range tokenSpecs {
		tokenValue := t.match(spec.re, current)

		if tokenValue == "" {
			continue
		}

		if spec.tokenType == TOK_IGNORE {
			return t.GetNextToken()
		}

		return Token{spec.tokenType, tokenValue}, nil
	}

	return Token{}, fmt.Errorf(`unexpected token: "%s"`, current[0:1])
}

func (t *Tokenizer) HasMoreTokens() bool {
	return t.cursor < len(t.input)
}

func (t *Tokenizer) match(re *regexp.Regexp, input string) string {
	matched := re.FindString(input)
	if matched == "" {
		return ""
	}

	t.cursor += len(matched)
	return matched
}

func NewTokenizer() Tokenizer {
	return Tokenizer{}
}

func init() {
	tokenSpecs = []tokenSpec{
		// ignored
		{regexp.MustCompile(`^\s+`), TOK_IGNORE},
		{regexp.MustCompile(`^\/\/.*`), TOK_IGNORE},
		{regexp.MustCompile(`^\/\*[\s\S]*\*\/`), TOK_IGNORE},

		// Symbols
		{regexp.MustCompile(`^\.`), TOK_DOT},
		{regexp.MustCompile(`^;`), TOK_SEMI},
		{regexp.MustCompile(`^\{`), TOK_LEFT_BRACKET_CURLY},
		{regexp.MustCompile(`^\}`), TOK_RIGHT_BRACKET_CURLY},
		{regexp.MustCompile(`^\(`), TOK_LEFT_PAREN},
		{regexp.MustCompile(`^\)`), TOK_RIGHT_PAREN},

		// Keywords
		{regexp.MustCompile(`^\bboolean\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bchar\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bclass\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bconstructor\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bdo\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\belse\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bfield\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bfunction\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bif\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bint\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bmethod\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\breturn\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bstatic\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bvar\b`), TOK_KEYWORD},
		{regexp.MustCompile(`^\bvoid\b`), TOK_KEYWORD},

		// Strings
		{regexp.MustCompile(`^"[^"]*"`), TOK_STRING},
		{regexp.MustCompile(`^'[^']*'`), TOK_STRING},

		// Numbers
		{regexp.MustCompile(`^\d+`), TOK_NUMBER},

		// Identifiers
		{regexp.MustCompile(`^\w+`), TOK_IDENTIFIER},
	}
}

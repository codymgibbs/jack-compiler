package parser

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

// Classes
func TestSimpleClass(t *testing.T) {
	input := `
		class Main {
		}
	`

	expect := &Class{
		[]Node{
			&Keyword{"class"},
			&Idenfier{"Main"},
			&Symbol{"{"},
			&Symbol{"}"},
		},
	}

	AssertParsedClassMatches(t, input, expect)
}

func TestClassWithVarDec(t *testing.T) {
	input := `
		class Main {
			static boolean test;
			field int count;
		}
	`

	expect := &Class{
		[]Node{
			&Keyword{"class"},
			&Idenfier{"Main"},
			&Symbol{"{"},
			&ClassVarDec{
				[]Node{
					&Keyword{"static"},
					&Keyword{"boolean"},
					&Idenfier{"test"},
					&Symbol{";"},
				},
			},
			&ClassVarDec{
				[]Node{
					&Keyword{"field"},
					&Keyword{"int"},
					&Idenfier{"count"},
					&Symbol{";"},
				},
			},
			&Symbol{"}"},
		},
	}

	AssertParsedClassMatches(t, input, expect)
}

func TestSubRoutineWithDoStatements(t *testing.T) {
	input := `
		class Main {
			function void more() {
				do game.run();
				do game.dispose();
			}
		}
	`

	expect := &Class{
		[]Node{
			&Keyword{"class"},
			&Idenfier{"Main"},
			&Symbol{"{"},
			&SubRoutineDec{
				[]Node{
					&Keyword{"function"},
					&Keyword{"void"},
					&Idenfier{"more"},
					&Symbol{"("},
					&ParameterList{},
					&Symbol{")"},
					&SubRoutineBody{
						[]Node{
							&Symbol{"{"},
							&Statements{
								[]Node{
									&DoStatement{
										[]Node{
											&Keyword{"do"},
											&Idenfier{"game"},
											&Symbol{"."},
											&Idenfier{"run"},
											&Symbol{"("},
											&ExpressionList{},
											&Symbol{")"},
											&Symbol{";"},
										},
									},
									&DoStatement{
										[]Node{
											&Keyword{"do"},
											&Idenfier{"game"},
											&Symbol{"."},
											&Idenfier{"dispose"},
											&Symbol{"("},
											&ExpressionList{},
											&Symbol{")"},
											&Symbol{";"},
										},
									},
								},
							},
							&Symbol{"}"},
						},
					},
				},
			},
			&Symbol{"}"},
		},
	}

	AssertParsedClassMatches(t, input, expect)
}

func TestSubRoutineWithIfStatements(t *testing.T) {
	input := `
		class Main {
			function void more() {
				var boolean b;
				if (b) {
				} else {
				}
				return;
			}
		}
	`

	expect := &Class{
		[]Node{
			&Keyword{"class"},
			&Idenfier{"Main"},
			&Symbol{"{"},
			&SubRoutineDec{
				[]Node{
					&Keyword{"function"},
					&Keyword{"void"},
					&Idenfier{"more"},
					&Symbol{"("},
					&ParameterList{},
					&Symbol{")"},
					&SubRoutineBody{
						[]Node{
							&Symbol{"{"},
							&VarDec{
								[]Node{
									&Keyword{"var"},
									&Keyword{"boolean"},
									&Idenfier{"b"},
									&Symbol{";"},
								},
							},
							&Statements{
								[]Node{
									&IfStatement{
										[]Node{
											&Keyword{"if"},
											&Symbol{"("},
											&Expression{
												[]Node{
													&Term{
														[]Node{
															&Idenfier{"b"},
														},
													},
												},
											},
											&Symbol{")"},
											&Symbol{"{"},
											&Statements{},
											&Symbol{"}"},
											&Keyword{"else"},
											&Symbol{"{"},
											&Statements{},
											&Symbol{"}"},
										},
									},
									&ReturnStatement{
										[]Node{
											&Keyword{"return"},
											&Symbol{";"},
										},
									},
								},
							},
							&Symbol{"}"},
						},
					},
				},
			},
			&Symbol{"}"},
		},
	}

	AssertParsedClassMatches(t, input, expect)
}

func AssertParsedClassMatches(t *testing.T, input string, expect *Class) {
	t.Helper()

	parser := NewParser()

	ast, err := parser.Parse(input)

	if err != nil {
		t.Error(err)
	}

	if diff := cmp.Diff(expect, ast); diff != "" {
		t.Errorf("Parsing mismatch (-want +got):\n%s", diff)
	}
}

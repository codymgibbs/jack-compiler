package parser

import "strings"

func JoinStringWithLast(list []string, join string, lastJoin string) string {
	if len(list) <= 0 {
		return ""
	}

	if len(list) == 1 {
		return list[0]
	}

	formatter := strings.Builder{}

	for i, v := range list {
		formatter.WriteString(v)

		if i+2 < len(list) {
			formatter.WriteString(join)
		} else if i+1 < len(list) {
			formatter.WriteString(lastJoin)
		}
	}

	return formatter.String()
}

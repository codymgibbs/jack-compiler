package parser

import "testing"

func TestStringListJoinWithLast_empty(t *testing.T) {
	result := JoinStringWithLast([]string{}, ", ", " or ")
	if result != "" {
		t.Error("Fail")
	}
}

func TestStringListJoinWithLast_1(t *testing.T) {
	result := JoinStringWithLast([]string{"foo"}, ", ", " or ")
	if result != "foo" {
		t.Error("Fail")
	}
}

func TestStringListJoinWithLast_2(t *testing.T) {
	result := JoinStringWithLast([]string{"foo", "bar"}, ", ", " or ")

	AssertEqual(t, "foo or bar", result)
}

func TestStringListJoinWithLast_3(t *testing.T) {
	result := JoinStringWithLast([]string{"foo", "bar", "baz"}, ", ", " or ")

	AssertEqual(t, "foo, bar or baz", result)
}

func AssertEqual(t *testing.T, expect string, got string) {
	t.Helper()
	if expect != got {
		t.Errorf(`Fail. Expected: "%s", got: "%s"`, expect, got)
	}
}

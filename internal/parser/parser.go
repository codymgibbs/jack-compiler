package parser

import (
	"fmt"

	"temporary.design/jack-compiler/internal/tokenizer"
)

type Parser struct {
	input     string
	tokenizer tokenizer.Tokenizer
	lookahead tokenizer.Token
}

func (p *Parser) isClassVarDecNext() bool {
	return p.lookahead.ContainsValueIn("static", "field")
}

func (p *Parser) isSubroutineDecNext() bool {
	return p.lookahead.ContainsValueIn("constructor", "function", "method")
}

func (p *Parser) isTypeKeywordNext() bool {
	return p.lookahead.ContainsValueIn("boolean", "char", "int")
}

// Parse input into a syntax tree
func (p *Parser) Parse(input string) (Node, error) {
	p.input = input
	p.tokenizer.Init(input)

	next, err := p.tokenizer.GetNextToken()
	if err != nil {
		return nil, err
	}

	p.lookahead = next

	root := Class{}
	return &root, p.ParseClass(&root)
}

// ParseClass
//
// class ::= 'class' className '{' classVarDec* subRoutineDec* '}'
func (p *Parser) ParseClass(parent Node) error {
	// 'class'
	if err := p.ParseKeyword(parent, "class"); err != nil {
		return err
	}

	// className
	if err := p.ParseIdentifier(parent); err != nil {
		return err
	}

	// '{'
	if err := p.ParseSymbol(parent, tokenizer.TOK_LEFT_BRACKET_CURLY); err != nil {
		return err
	}

	// classVarDec*
	if err := p.ParseNodeListInto(parent, p.isClassVarDecNext, p.ParseClassVarDec); err != nil {
		return err
	}

	// subRoutineDec*
	if err := p.ParseNodeListInto(parent, p.isSubroutineDecNext, p.ParseSubroutineDec); err != nil {
		return err
	}

	// '}'
	if err := p.ParseSymbol(parent, tokenizer.TOK_RIGHT_BRACKET_CURLY); err != nil {
		return err
	}

	return nil
}

// ParseClassVarDec
//
// classVarDec ::= ('static' | 'field') type varName (',' varName)* ';'
func (p *Parser) ParseClassVarDec(parent Node) error {
	n := ClassVarDec{}
	parent.PushNode(&n)

	// ('static' | 'field')
	if err := p.ParseKeyword(&n, "static", "field"); err != nil {
		return err
	}

	// type
	if err := p.ParseType(&n); err != nil {
		return err
	}

	// varName
	if err := p.ParseIdentifier(&n); err != nil {
		return err
	}

	// TODO: (',', varName)*

	// ';'
	if err := p.ParseSymbol(&n, tokenizer.TOK_SEMI); err != nil {
		return err
	}

	return nil
}

// ParseDoStatement
//
// doStatement ::= 'do' subroutineCall ';'
func (p *Parser) ParseDoStatement(parent Node) error {
	n := DoStatement{}
	parent.PushNode(&n)

	// 'do'
	if err := p.ParseKeyword(&n, "do"); err != nil {
		return err
	}

	// subroutineCall
	if err := p.ParseSubroutineCall(&n); err != nil {
		return err
	}

	// ';'
	if err := p.ParseSymbol(&n, tokenizer.TOK_SEMI); err != nil {
		return err
	}

	return nil
}

// ParseExpression
//
// expression ::= term (op term)*
func (p *Parser) ParseExpression(parent Node) error {
	n := Expression{}
	parent.PushNode(&n)

	// term
	if err := p.ParseTerm(&n); err != nil {
		return err
	}

	// (...)*

	// |-> op
	// |-> term

	return nil
}

// ParseIdentifier
//
// identifer ::= "token of type: Identifer"
func (p *Parser) ParseIdentifier(parent Node) error {
	t, err := p.eat(tokenizer.TOK_IDENTIFIER)
	if err != nil {
		return err
	}

	parent.PushNode(&Idenfier{t.Value})

	return nil
}

// ParseIfStatement
//
// ifStatement ::= 'if' '(' expression ')' '{' statements '}'
// 				   ('else' '{' statements '}')?
func (p *Parser) ParseIfStatement(parent Node) error {
	n := IfStatement{}
	parent.PushNode(&n)

	// 'if'
	if err := p.ParseKeyword(&n, "if"); err != nil {
		return err
	}

	// '('
	if err := p.ParseSymbol(&n, tokenizer.TOK_LEFT_PAREN); err != nil {
		return err
	}

	// expression
	if err := p.ParseExpression(&n); err != nil {
		return err
	}

	// ')'
	if err := p.ParseSymbol(&n, tokenizer.TOK_RIGHT_PAREN); err != nil {
		return err
	}

	// '{'
	if err := p.ParseSymbol(&n, tokenizer.TOK_LEFT_BRACKET_CURLY); err != nil {
		return err
	}

	// statements
	if err := p.ParseStatementList(&n); err != nil {
		return err
	}

	// '}'
	if err := p.ParseSymbol(&n, tokenizer.TOK_RIGHT_BRACKET_CURLY); err != nil {
		return err
	}

	// (...)?
	if p.lookahead.ContainsValueIn("else") {
		// 'else'
		if err := p.ParseKeyword(&n, "else"); err != nil {
			return err
		}

		// '{'
		if err := p.ParseSymbol(&n, tokenizer.TOK_LEFT_BRACKET_CURLY); err != nil {
			return err
		}

		// statements
		if err := p.ParseStatementList(&n); err != nil {
			return err
		}

		// '}'
		if err := p.ParseSymbol(&n, tokenizer.TOK_RIGHT_BRACKET_CURLY); err != nil {
			return err
		}
	}

	return nil
}

// ParseKeyword
//
// symbol ::= "token of type: Symbol"
func (p *Parser) ParseKeyword(parent Node, options ...string) error {
	t, err := p.eatValue(tokenizer.TOK_KEYWORD, options)
	if err != nil {
		return err
	}

	parent.PushNode(&Keyword{t.Value})
	return nil
}

// ParseReturnStatement
//
// returnStatement ::= 'return' expression? ';'
func (p *Parser) ParseReturnStatement(parent Node) error {
	// 'return'
	// nReturn, err := p.ParseKeyword("return")
	if err := p.ParseKeyword(parent, "return"); err != nil {
		return err
	}

	// expression?
	// nExpr, err := p.ParseExpression()
	// if err != nil {
	// 	return nil, err
	// }
	// n.PushNode(nExpr)

	// ';'
	if err := p.ParseSymbol(parent, tokenizer.TOK_SEMI); err != nil {
		return err
	}

	return nil
}

// ParseStatement
//
// statement ::= letStatement | ifStatement | whileStatement | doStatement | returnStatement
func (p *Parser) ParseStatement(parent Node) error {
	switch p.lookahead.Value {
	case "if":
		return p.ParseIfStatement(parent)
	case "do":
		return p.ParseDoStatement(parent)
	// case "let":
	// 	return p.ParseLetStatement()
	// case "let":
	// 	return p.ParseLetStatement()
	default:
		dest := ReturnStatement{}
		parent.PushNode(&dest)
		return p.ParseReturnStatement(&dest)
	}
}

// ParseStatementList
//
// statements ::= statement*
func (p *Parser) ParseStatementList(parent Node) error {
	sl := Statements{}
	parent.PushNode(&sl)

	statementPrefixes := []string{"let", "if", "while", "do", "return"}

	if !p.lookahead.ContainsValueIn(statementPrefixes...) {
		return nil
	}

	return p.ParseNodeListInto(
		&sl,
		func() bool {
			return p.lookahead.ContainsValueIn(statementPrefixes...)
		},
		p.ParseStatement,
	)
}

// ParseSubroutineBody
//
// subroutineBody ::= '{' varDec* statements '}'
func (p *Parser) ParseSubroutineBody(parent Node) error {
	n := SubRoutineBody{}
	parent.PushNode(&n)

	// '{'
	if err := p.ParseSymbol(&n, tokenizer.TOK_LEFT_BRACKET_CURLY); err != nil {
		return err
	}

	// varDec*
	err := p.ParseNodeListInto(
		&n,
		func() bool {
			return p.lookahead.Value == "var"
		},
		p.ParseVarDec,
	)
	if err != nil {
		return err
	}

	// statement*
	if err = p.ParseStatementList(&n); err != nil {
		return err
	}

	// '}'
	if err := p.ParseSymbol(&n, tokenizer.TOK_RIGHT_BRACKET_CURLY); err != nil {
		return err
	}

	return nil
}

// ParseSubroutineCall
//
// subroutineCall ::= subroutineName '(' expressionList ')'
//					| (className | varName) '.' subroutineName '(' expressionList ')'
func (p *Parser) ParseSubroutineCall(parent Node) error {
	// subroutineName | (className | varName)
	if err := p.ParseIdentifier(parent); err != nil {
		return err
	}

	// ('.' subroutineName)?
	if p.lookahead.Value == "." {
		// '.'
		if err := p.ParseSymbol(parent, tokenizer.TOK_DOT); err != nil {
			return err
		}

		// subroutineName
		if err := p.ParseIdentifier(parent); err != nil {
			return err
		}
	}

	// '('
	if err := p.ParseSymbol(parent, tokenizer.TOK_LEFT_PAREN); err != nil {
		return err
	}

	// expressionList
	// TODO Impl expressionList
	parent.PushNode(&ExpressionList{})

	// ')'
	if err := p.ParseSymbol(parent, tokenizer.TOK_RIGHT_PAREN); err != nil {
		return err
	}

	return nil
}

// ParseSubroutineDec
//
// subroutineDec ::= ('constructor' | 'function' | 'method') ('void' | type) subroutineName '(' parameterList ')' subroutineBody
func (p *Parser) ParseSubroutineDec(parent Node) error {
	n := SubRoutineDec{}
	parent.PushNode(&n)

	// ('constructor' | 'function' | 'method')
	a, err := p.eatValue(tokenizer.TOK_KEYWORD, []string{"constructor", "function", "method"})
	if err != nil {
		return err
	}
	n.PushNode(&Keyword{a.Value})

	// ('void' | type)
	if p.lookahead.Type == tokenizer.TOK_KEYWORD {
		if err := p.ParseKeyword(&n, "void"); err != nil {
			return err
		}
	} else {
		if err := p.ParseType(&n); err != nil {
			return err
		}
	}

	// subroutineName
	if err := p.ParseIdentifier(&n); err != nil {
		return err
	}

	// '('
	if err := p.ParseSymbol(&n, tokenizer.TOK_LEFT_PAREN); err != nil {
		return err
	}

	// parameterList
	pl := ParameterList{}
	n.PushNode(&pl)

	// ')'
	if err := p.ParseSymbol(&n, tokenizer.TOK_RIGHT_PAREN); err != nil {
		return err
	}

	// subroutineBody
	if err = p.ParseSubroutineBody(&n); err != nil {
		return err
	}

	return nil
}

// ParseSymbol
//
// symbol ::= "token of type: Symbol"
func (p *Parser) ParseSymbol(parent Node, value tokenizer.TokenType) error {
	t, err := p.eat(value)
	if err != nil {
		return err
	}

	parent.PushNode(&Symbol{t.Value})

	return nil
}

// ParseTerm
//
// term ::= integerConstant
//			| stringConstant
//			| keywordConstant
// 			| varName
// 			| varName '[' expression ']'
//			| subroutineCall
//			| '(' expression ')'
//			| unaryOp term
func (p *Parser) ParseTerm(parent Node) error {
	n := Term{}
	parent.PushNode(&n)

	switch p.lookahead.Type {
	case tokenizer.TOK_IDENTIFIER:
		if err := p.ParseIdentifier(&n); err != nil {
			return err
		}
	}

	return nil
}

// ParseType
//
// type ::= 'int' | 'char' | 'boolean' | className
func (p *Parser) ParseType(parent Node) error {
	typeKeywords := []string{"int", "car", "boolean"}

	if p.lookahead.ContainsValueIn(typeKeywords...) {
		// 'int' | 'char' | 'boolean'
		t, err := p.eatValue(tokenizer.TOK_KEYWORD, typeKeywords)
		if err != nil {
			return err
		} else {
			parent.PushNode(&Keyword{t.Value})
			return nil
		}
	} else {
		// className
		if err := p.ParseIdentifier(parent); err != nil {
			return err
		}

		return nil
	}
}

// VarDec
//
// varDec ::= 'var' Type|Identifier varName (',' varName)* ';'
func (p *Parser) ParseVarDec(parent Node) error {
	n := VarDec{}
	parent.PushNode(&n)

	// 'var'
	tkVar, err := p.eatValue(tokenizer.TOK_KEYWORD, []string{"var"})
	if err != nil {
		return err
	}
	n.PushNode(&Keyword{tkVar.Value})

	// Type|Identifier
	if p.isTypeKeywordNext() {
		if err := p.ParseType(&n); err != nil {
			return err
		}
	} else {
		if err := p.ParseIdentifier(&n); err != nil {
			return err
		}
	}

	// varName
	if err := p.ParseIdentifier(&n); err != nil {
		return err
	}

	// (',' varName)*
	// TODO Support multiple varNames

	// ';'
	if err := p.ParseSymbol(&n, tokenizer.TOK_SEMI); err != nil {
		return err
	}

	return nil
}

// ParseNodeListInto
func (p *Parser) ParseNodeListInto(parent Node, lookaheadTest func() bool, parser func(Node) error) error {
	for {
		if !lookaheadTest() {
			break
		}

		if err := parser(parent); err != nil {
			return err
		}
	}

	return nil
}

func (p *Parser) eatValue(tokenType tokenizer.TokenType, options []string) (*tokenizer.Token, error) {
	tok, err := p.eat(tokenType)
	if err != nil {
		return nil, err
	}

	if tok.ContainsValueIn(options...) {
		return tok, nil
	} else {
		return nil, fmt.Errorf(
			`parser error: expected %s, got "%s"`,
			JoinStringWithLast(options, ", ", " or "),
			p.lookahead.Value,
		)
	}
}

func (p *Parser) eat(tokenType tokenizer.TokenType) (*tokenizer.Token, error) {
	token := p.lookahead

	if token.Type == tokenizer.TOK_IGNORE {
		return nil, fmt.Errorf(`unexpected end of input, expected: "%s"`, tokenType.ToString())
	}

	if token.Type != tokenType {
		return nil, fmt.Errorf(`unexpected token { type: "%s", value: "%s" }, expected "%s"`,
			token.Type.ToString(),
			token.Value,
			tokenType.ToString(),
		)
	}

	next, err := p.tokenizer.GetNextToken()
	if err != nil {
		return nil, err
	}

	p.lookahead = next

	return &token, nil
}

// NewParser constructs a new parser
func NewParser() Parser {
	return Parser{"", tokenizer.NewTokenizer(), tokenizer.Token{}}
}

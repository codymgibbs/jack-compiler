package parser

type Node interface {
	Nodes() []Node
	PushNode(Node)
}

// Lexical Elements (aka Terminals)

type Constant interface {
	Constant() interface{}
}

type ConstantInteger struct {
	Value int
}

func (n *ConstantInteger) Constant() interface{} {
	return n
}

type ConstantString struct {
	Value string
}

func (n *ConstantString) Constant() interface{} {
	return n
}

type Idenfier struct {
	Value string
}

func (n *Idenfier) Nodes() []Node {
	return nil
}

func (n *Idenfier) PushNode(node Node) {
}

type Keyword struct {
	Value string
}

func (n *Keyword) Nodes() []Node {
	return nil
}

func (n *Keyword) PushNode(node Node) {
}

type Symbol struct {
	Value string
}

func (n *Symbol) Nodes() []Node {
	return nil
}

func (n *Symbol) PushNode(node Node) {
}

// Program Structure

type Class struct {
	Children []Node
}

func (n *Class) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

func (n *Class) Nodes() []Node {
	return n.Children
}

type ClassVarDec struct {
	Children []Node
}

func (n *ClassVarDec) Nodes() []Node {
	return n.Children
}

func (n *ClassVarDec) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type ParameterList struct {
	Children []Node
}

func (n *ParameterList) Nodes() []Node {
	return n.Children
}

func (n *ParameterList) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type SubRoutineBody struct {
	Children []Node
}

func (n *SubRoutineBody) Nodes() []Node {
	return n.Children
}

func (n *SubRoutineBody) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type SubRoutineDec struct {
	Children []Node
}

func (n *SubRoutineDec) Nodes() []Node {
	return n.Children
}

func (n *SubRoutineDec) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type Type struct {
	Value string
}

func (n *Type) Nodes() []Node {
	return nil
}

func (n *Type) PushNode(node Node) {
}

type VarDec struct {
	Children []Node
}

func (n *VarDec) Nodes() []Node {
	return n.Children
}

func (n *VarDec) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

// Statements

type DoStatement struct {
	Children []Node
}

func (n *DoStatement) Nodes() []Node {
	return n.Children
}

func (n *DoStatement) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type IfStatement struct {
	Children []Node
}

func (n *IfStatement) Nodes() []Node {
	return n.Children
}

func (n *IfStatement) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type ReturnStatement struct {
	Children []Node
}

func (n *ReturnStatement) Nodes() []Node {
	return n.Children
}

func (n *ReturnStatement) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type Statements struct {
	Children []Node
}

func (n *Statements) Nodes() []Node {
	return n.Children
}

func (n *Statements) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

// type Statement interface {
// 	Statement() interface{}
// }

// type BlockStatement struct {
// 	Body []Statement
// }

// func (n *BlockStatement) Statement() interface{} {
// 	return n
// }

// type ExpressionStatement struct {
// 	Value Constant
// }

// func (n *ExpressionStatement) Statement() interface{} {
// 	return n
// }

// Expressions

type Expression struct {
	Children []Node
}

func (n *Expression) Nodes() []Node {
	return n.Children
}

func (n *Expression) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type ExpressionList struct {
	Children []Node
}

func (n *ExpressionList) Nodes() []Node {
	return n.Children
}

func (n *ExpressionList) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

type Term struct {
	Children []Node
}

func (n *Term) Nodes() []Node {
	return n.Children
}

func (n *Term) PushNode(node Node) {
	n.Children = append(n.Children, node)
}

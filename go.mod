module temporary.design/jack-compiler

go 1.16

require (
	github.com/go-test/deep v1.0.7
	github.com/google/go-cmp v0.5.6 // indirect
)
